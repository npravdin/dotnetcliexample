# Instructions

0. Must use dotnet core as specified in `csproj` file
1. Fix up source code to have the same behaviour as what is listed below in Example Usage setting.
2. Validate input arguments (e.g. that src/dest are strings)
3. Given an input config file in YAML format, ensure it can be loaded into the `AppConfig` class
4. Given the `generate` command, create a valid config file in YAML format that can be consumed by a subsequent `consume` command.
5. Write Unit tests using [Xunit](https://xunit.github.io/)
6. Generate a unit test report demonstrating code coverage > 90%
7. Provide build, test, and run commands to demonstrate correctness.
8. Create a pull request against the repo.  Task will be considered complete when code is merged.


## Example Usage


### With no parameters specified
```bash
./dotnetcoreexample 
Usage: dotnetcoreexaple [OPTIONS] COMMAND [ARGS]...

  An example CLI program in dotnet core

Options:
  --help  Show this message and exit.

Commands:
  generate           Generate a callibration
  consume            Consume a callibration
```

### Example generate

```bash
./dotnetcoreexample
Usage: dotnetcoreexaple generate [OPTIONS]

  Generate a callibration

Options:
  --src TEXT   Specify the location for input
  --dest TEXT  Specify the destination for the configuration file
  --help       Show this message and exit.
```


### Example consume command

```bash
./dotnetcoreexample
Usage: dotnetcoreexaple consume [OPTIONS]

  Consume a config file

Options:
  --config TEXT  Path to config file
  --src TEXT     Specify the location for (other) input
  --dest TEXT    Specify the location for output
  --help         Show this message and exit.
```

